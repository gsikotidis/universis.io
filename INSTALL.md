# Installation
Our webpage is built using gatsby! Follow the instructions to build and run it!

## Install dependencies

`npm i`

## Build it

`npm run build`

## Run it

`npm start`

## See it

http://localhost:8000/
