This repo includes the code to run https://universis.io
Our webpage is built using gatsby - here's how to [build and run it](https://gitlab.com/universis/universis.io/blob/master/INSTALL.md).

# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open s
ource platform. The target is to serve our common needs to support academic and administrative processes.

The UniverSIS platform consists of the following:

* Core – the models defining the backend schema, providing database agnostic persistence (future development)
* [API](https://gitlab.com/universis/universis-api) – the data backend methods to abstract the persistence layer
* [OAUTH server](https://gitlab.com/universis/universis-auth) – the authentication/authorization server to provide access to the API
* [Students](https://gitlab.com/universis/universis-students) – the front-end interface for all student interaction (under testing)
* [Teachers](https://gitlab.com/universis/universis-teachers) – the front-end interface for all teacher interaction (under development)
* [Registrar](https://gitlab.com/universis/universis) – the back-office application for all Registrar processes (under development)

## Tech stack
* [Nodejs](https://nodejs.org/en/)
* [Typescript](https://github.com/Microsoft/TypeScript)
* [Angular](https://angular.io/)
* [ExpressJS](https://github.com/expressjs/express)
* [@theMOST](https://github.com/themost-framework/themost)
* [CoreUI](https://github.com/coreui)
* [Bootstrap](https://github.com/twbs/bootstrap/)

## Demo
* Check out our [Students demo](https://gitlab.com/universis/universis-students#demo)
* Check out our [Teachers demo](https://gitlab.com/universis/universis-teachers#demo)

## Installation
To install all currently available parts check:
* [API](https://gitlab.com/universis/universis-api)
* [OAUTH](https://gitlab.com/universis/universis-auth)
* [Students](https://gitlab.com/universis/universis-students/blob/master/INSTALL.md)
* [Teachers](https://gitlab.com/universis/universis-teachers/blob/master/INSTALL.md)

## Questions?

Check our wiki [FAQ](https://gitlab.com/universis/universis.io/wikis/FAQ) page, and feel free to [contact us](https://www.universis.gr/#con
tact).
