import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import version from "../assets/images/undraw_version_control_9bpv.svg";
import upgrade from "../assets/images/undraw_upgrade_06a0.svg";
import scrum from "../assets/images/undraw_scrum_board_cesn.svg";
import { ACTION_TEXTS } from "../assets/data/languages";

const Action = props => (
  <Fragment>
    <section className="text-center bg--secondary">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8" id="action">
            <h2>Entities</h2>
            <p className="lead">
              The UniverSIS platform consists of the following:
            </p>
          </div>
        </div>
      </div>
    </section>
    <section className="bg--secondary">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="feature feature-3 boxed boxed--lg boxed--border text-center">
              <img className="icon-three-2" src={version} alt="" />
              <h4 className="type--bold">{ACTION_TEXTS.open.en}</h4>
              <br />
              <p className="lead">{ACTION_TEXTS.openSub.en}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-3 boxed boxed--lg boxed--border text-center">
              <img className="icon-three-2" src={scrum} alt="" />
              <h4 className="type--bold">{ACTION_TEXTS.synergy.en}</h4>
              <br />
              <p className="lead">{ACTION_TEXTS.synergySub.en}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-3 boxed boxed--lg boxed--border text-center">
              <img className="icon-three-2" src={upgrade} alt="" />
              <h4 className="type--bold">{ACTION_TEXTS.modern.en}</h4>
              <br />
              <p className="lead">{ACTION_TEXTS.modernSub.en}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

export default Action;
